#!/bin/sh
VERSION="0.27b"
FILES="cpudebug.c cpudebug.h star.c starcpu.h stardoc.txt"

tar -cvjf starscream-$VERSION.tar.bz2 $FILES

mkdir dos
cp $FILES dos
cd dos
recode iso-8859-1..iso-8859-1/CR-LF $FILES
zip ../starscream-$VERSION.zip $FILES
cd ..
rm -rf dos

