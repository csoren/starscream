#include <stdio.h>
#include <stdarg.h>
#include <memory.h>
#include "../starcpu.h"

unsigned short mem1[32768], mem2[32768];
unsigned char  mem3[0x100];

#define IO_BEGIN1 0x100000
#define IO_END1   0x1000FF
#define IO_BEGIN2 0x100100
#define IO_END2   0x1001FF
#define IO_BEGIN3 0x100200
#define IO_END3   0x1002FF

void assert(int b, const char *fmt, ...)
{
	if(b)
		printf("Passed: ");
	else
		printf("**FAILED**: ");

	va_list v;
	va_start(v, fmt);
	vprintf(fmt, v);
	va_end(v);
}

void initmemarray(unsigned short *mem, int length, int content)
{
	while(length--)
		*mem++ = content++;
}

void initmem()
{
	initmemarray(mem1, 32768, 0);
	initmemarray(mem2, 32768, 32768);
}

void writeword(int address, unsigned short d)
{
	if(address < 65536)
		mem1[address>>1] = d;
	else
		mem2[(address-65536)>>1] = d;
}

void writeint(int address, unsigned int d)
{
	writeword(address, d >> 16);
	writeword(address + 2, d & 0xFFFF);
}

unsigned int readword(int address)
{
	if(address < 65536)
		return mem1[address>>1];
	else
		return mem2[(address-65536)>>1];
}

unsigned int readlong(int address)
{
	return (readword(address) << 16) | readword(address + 2);
}

unsigned cbreadbyte(unsigned address)
{
	printf("Reading byte from %08X\n", address);
	return address&0xFF;
}

void cbwritebyte(unsigned address, unsigned data)
{
	printf("Write byte %02X to %08X\n", data, address);
	switch(address)
	{
		case IO_BEGIN1 + 0x00:
			s68000releaseTimeslice();
			break;
		default:
			break;
	}
}

unsigned cbreadword(unsigned address)
{
	printf("Reading word from %08X\n", address);
	unsigned r = address&0xFF;
	return r | (r << 8);
}

void cbwriteword(unsigned address, unsigned data)
{
	printf("Write word %04X to %08X\n", data, address);
	switch(address)
	{
		default:
			break;
	}
}

int main(int argc, char *argv[])
{
	int i;
	int ctxsz = s68000GetContextSize();
	assert(ctxsz == sizeof(s68000context), "Context size returned = %d, expected %ld\n", ctxsz, sizeof(s68000context));

	s68000init();
	
	struct STARSCREAM_PROGRAMREGION fetch[] =
	{
		{ 0x00000, 0x0FFFF, mem1 - 0 },
		{ 0x10000, 0x1FFFF, mem2 - 65536 / sizeof(short)},
		{      -1,      -1, NULL }
	};

	struct STARSCREAM_DATAREGION    readbyte[] =
	{
		{ 0x00000,   0x0FFFF, NULL, mem1 },
		{ 0x10000,   0x1FFFF, NULL, mem2 },
		{ IO_BEGIN1, IO_END1, cbreadbyte, NULL },
		{ IO_BEGIN2, IO_END2, NULL, mem3 },
		{ IO_BEGIN3, IO_END3, cbreadbyte, NULL },
		{        -1,      -1, NULL, NULL }
	};
	struct STARSCREAM_DATAREGION    readword[] =
	{
		{ 0x00000, 0x0FFFF, NULL, mem1 },
		{ 0x10000, 0x1FFFF, NULL, mem2 },
		{ IO_BEGIN1, IO_END1, cbreadword, NULL },
		{ IO_BEGIN2, IO_END2, NULL, mem3 },
		{ IO_BEGIN3, IO_END3, cbreadword, NULL },
		{      -1,      -1, NULL, NULL }
	};
	struct STARSCREAM_DATAREGION    writebyte[] =
	{
		{ 0x00000,   0x0FFFF, NULL, mem1 },
		{ 0x10000,   0x1FFFF, NULL, mem2 },
		{ IO_BEGIN1, IO_END1, cbwritebyte, NULL },
		{ IO_BEGIN2, IO_END2, NULL, mem3 },
		{ IO_BEGIN3, IO_END3, cbwritebyte, NULL },
		{      -1,      -1, NULL, NULL }
	};
	struct STARSCREAM_DATAREGION    writeword[] =
	{
		{ 0x00000, 0x0FFFF, NULL, mem1 },
		{ 0x10000, 0x1FFFF, NULL, mem2 },
		{ IO_BEGIN1, IO_END1, cbwriteword, NULL },
		{ IO_BEGIN2, IO_END2, NULL, mem3 },
		{ IO_BEGIN3, IO_END3, cbwriteword, NULL },
		{      -1,      -1, NULL, NULL }
	};
	
	struct S68000CONTEXT ctx;
	memset(&ctx, 0, sizeof(ctx));
	ctx.fetch = fetch;
	ctx.u_fetch = fetch;
	ctx.s_fetch = fetch;
	ctx.readbyte = readbyte;
	ctx.u_readbyte = readbyte;
	ctx.s_readbyte = readbyte;
	ctx.readword = readword;
	ctx.u_readword = readword;
	ctx.s_readword = readword;
	ctx.writebyte = writebyte;
	ctx.u_writebyte = writebyte;
	ctx.s_writebyte = writebyte;
	ctx.writeword = writeword;
	ctx.u_writeword = writeword;
	ctx.s_writeword = writeword;
	
	s68000SetContext(&ctx);
	assert(memcmp(&ctx,&s68000context,sizeof(ctx)) == 0, "Stored context is the expected\n");
	
	struct S68000CONTEXT ctx2;
	s68000GetContext(&ctx2);
	assert(memcmp(&ctx,&ctx2,sizeof(ctx)) == 0, "Context returned is the expected\n");
	
	/* Test fetch */
	
	initmem();
	
	for(i = 0; i < 65536; ++i)
	{
		int v = s68000fetch(i*2);
		if(v != i)
			assert(v == i, "Value at memory %d should be %d, it's %d\n", i*2, i, v);
	}
	
	return 0;
}

